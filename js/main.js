$(document).ready(function() {
  //main banner
  var swiper = new Swiper(".swiper-container", {
    pagination: {
      el: ".swiper-pagination",
      type: "bullets",
      clickable: true
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    },
    autoplay: {
      delay: 5000
    }
  });

  // request
  // 연락처 하이픈 자동 입력
  function autoHypenPhone(str) {
    str = str.replace(/[^0-9]/g, "");
    var tmp = "";
    if (str.length < 4) {
      return str;
    } else if (str.length < 7) {
      tmp += str.substr(0, 3);
      tmp += "-";
      tmp += str.substr(3);
      return tmp;
    } else if (str.length < 11) {
      tmp += str.substr(0, 3);
      tmp += "-";
      tmp += str.substr(3, 3);
      tmp += "-";
      tmp += str.substr(6);
      return tmp;
    } else {
      tmp += str.substr(0, 3);
      tmp += "-";
      tmp += str.substr(3, 4);
      tmp += "-";
      tmp += str.substr(7);
      return tmp;
    }
    return str;
  }

  var cellPhone = document.getElementById("phone");
  cellPhone.onkeyup = function(event) {
    event = event || window.event;
    var _val = this.value.trim();
    this.value = autoHypenPhone(_val);
  };

  // 메일
  const $formContact = document.querySelector("#request__form");

  const sendForm = event => {
    event.preventDefault();
    const message = {
      company: document.querySelector("#company").value,
      name: document.querySelector("#name").value,
      email: document.querySelector("#email").value,
      phone: document.querySelector("#phone").value,
      detail: document.querySelector("#detail").value,
      subject: document.querySelector("#company").value + "에서 의뢰합니다."
    };
    smtpJS(message);
  };

  const smtpJS = message => {
    try {
      Email.send({
        //beanzsoft.co.kr 보안토큰
        SecureToken: "305f87a3-bcf2-4b79-8948-1f683dd87fdb",
        To: "beanzsoftai@gmail.com",
        From: message.email,
        Subject: message.subject,
        Body: `<p>회사명: ${message.company}</p>
        <p>담당자:  ${message.name}</p>
        <p>연락처: ${message.phone}</p>
        <p>이메일: ${message.email}</p>       
        <p>상세 의뢰 내용: ${message.detail}</p>`
      });
      alert("프로젝트 의뢰가 정상적으로 접수되었습니다.");
    } catch (err) {
      alert("Erro");
    }
    location.reload();
  };
  $formContact.addEventListener("submit", sendForm);
});
